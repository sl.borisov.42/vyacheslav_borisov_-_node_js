var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.set('view engine', 'ejs');

app.use('/public', express.static('public'));

app.get('/', function(req, res) {
    res.render('index');
});

app.get('/about', function(req, res) {
    res.render('about');
});

app.post('/about', urlencodedParser, function(req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log(req.body);
    res.render('about-success', {data: req.body});
});

app.get('/news/:id', function(req, res) {
    var obj = {title: "Новость", id: 4, pars: ['Параграф', 'Обыный текст', 'Числа: 2, 4, 6', 99]}
    console.log(req.query)
    res.render('news', {newsID: req.params.id, newsParam: '26.09.2018', obj: obj});
});

app.listen(3000);


//На самом деле, тут мало пояснений, потому, что я забываю, и не особо понимаю что надо пояснять
//Уроки, вроде бы, понял
//---
//Конец