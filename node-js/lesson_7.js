//---Видео 7---
var fs = require('fs');

fs.writeFileSync('./file', "bla-bla-bla");
setTimeout(function () {
    fs.unlink('file', function() {});
}, 1000);

fs.mkdir('new', function() {
    fs.writeFile('./new/some.txt', 'Что-то', function() {
        console.log("Всё сработало!");
    });
});

setTimeout(function () {
    fs.unlink('./new/some.txt', function() {});
    fs.rmdir('new', function() {});
}, 1500);