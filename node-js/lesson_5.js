//---Видео 5---

var events = require('events');
var util = require('util');

/*var myEmit = new events.EventEmitter();

myEmit.on('some_event', function(text) {
    console.log(text);
});

myEmit.emit('some_event', "It's worked");*/

var Cars = function(model) {
    this.model = model;
}

util.inherits(Cars, events.EventEmitter);

var bmw = new Cars('BMW');
var kia = new Cars('KIA');
var lada = new Cars('Lada');

var cars = [bmw, kia, lada];
cars.forEach(function(car) {
    car.on('speed', function(text) {
        console.log (`${car.model} speed is - ${text}`);
    });
});

bmw.emit('speed', '234 km');
lada.emit('speed', 'over9999 km');
